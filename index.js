
const laptopSelectElements = document.getElementById("laptops");
const laptopImageElement = document.getElementById("laptop-image");
const laptopTitleElement = document.getElementById("laptop-title");
const laptopSpecsElement = document.getElementById("laptop-specs");
const laptopPriceElement = document.getElementById("laptop-price")
const laptopDescriptionElement = document.getElementById("laptop-description")
const laptopFeaturesTitleElement = document.getElementById("features")
const laptopCardMiddleRowElement = document.getElementById("laptops-card-middle-row")
const laptopCardBuyNowButton = document.getElementById("buy-now-button")
const bankCardLoanRowElement = document.getElementById("bank-card-loan-row")
const bankCardBalanceElement = document.getElementById("bank-balance")
const bankCardLoanButton = document.getElementById("loan-button")
const bankCardOutstandingLoanElement = document.getElementById("outstanding-loan-balance")
const workCardWorkButton = document.getElementById("work-button")
const workCardPayBalanceElement = document.getElementById("pay-balance")
const workCardBankButton = document.getElementById("bank-button")
const workCardRepayLoanRowElement = document.getElementById("repay-loan-row")
const workCardRepayLoanButton = document.getElementById("repay-loan-button")

const laptops = [];
let bankBalance = 0;
let payBalance = 0;
let outstandingLoan = 0;
let loanTaken = false;
let selectedLaptopId = -1
let selectedLaptopPrice = 0

fetch("https://noroff-komputer-store-api.herokuapp.com/computers")
  .then((response) => {
    return response.json()
  })
  .then((data) => {
    console.log(data)
    laptops.push(...data)
    renderLaptops(data)
  })

function renderLaptops(laptops) {
  for (const laptop of laptops) {
    laptopSelectElements.innerHTML += `<option value=${laptop.id}>${laptop.title}</option>`
  }
}

function renderLaptop(selectedLaptop) {
    laptopSpecsElement.innerHTML = ''
    imageString = selectedLaptop.image
    laptopImageElement.src = "https://noroff-komputer-store-api.herokuapp.com/" + selectedLaptop.image
    laptopImageElement.alt = selectedLaptop.title
    laptopTitleElement.innerText = `${selectedLaptop.title}`
    laptopDescriptionElement.innerText = `${selectedLaptop.description}`
    laptopCardBuyNowButton.setAttribute('style', 'display:inline')
    laptopPriceElement.innerText = `${selectedLaptop.price}` + ' NOK'


    for (const key in selectedLaptop.specs) {
        const specs = selectedLaptop.specs[key]
        laptopSpecsElement.innerHTML += `<li>${specs}</li>`
    }
}

function onSelectChange() {
    laptopFeaturesTitleElement.setAttribute('style', 'display:inline')
    laptopCardMiddleRowElement.style.margin = "5% 0 10px 0"
    const laptopId = this.value
    const selectedLaptop = laptops.find(g => g.id == laptopId)
    selectedLaptopId = selectedLaptop.id
    selectedLaptopPrice = selectedLaptop.price
    if(bankBalance >= selectedLaptopPrice) {
        laptopCardBuyNowButton.disabled = false
    } else {
        laptopCardBuyNowButton.disabled = true
    }
    for (var i = 0; i < laptopSelectElements.length; i++) {
        let option = laptopSelectElements.options[i]
        if(option.value == "-1") {
            laptopSelectElements.removeChild(option)
        }
    }
    renderLaptop(selectedLaptop)
}   

function onClickLoanButton() {

    if(loanTaken) {
        alert("Sorry, you can only have one loan at a time. Repay your current loan before applying for a new one. ")
    }
    
    if(!loanTaken) {
        let amount = prompt("Please enter the amount you want to loan: ")
        if(amount != null && !(isNaN(Number(amount))) && amount != "") {
            if(Number(amount) <= (bankBalance * 2)) {
                outstandingLoan = Number(amount)
                bankCardOutstandingLoanElement.innerText = outstandingLoan + ' kr.'
                loanTaken = true
                bankBalance = bankBalance + outstandingLoan
                bankCardBalanceElement.innerText = bankBalance + ' kr.'
                if(bankBalance >= selectedLaptopPrice) {
                    laptopCardBuyNowButton.disabled = false
                } else {
                    laptopCardBuyNowButton.disabled = true
                }
                alert("Congratulations! Your loan has been approved. ")
                bankCardLoanRowElement.setAttribute('style', 'display:inline')
                workCardRepayLoanRowElement.setAttribute('style', 'display:inline')
                workCardRepayLoanButton.style.margin = "5% 0 4px 0"
                workCardRepayLoanButton.disabled = true
            } else {
                alert("You are not eligible to loan more than twice your bank-balance. ")
            }
        } else {
            alert("Please enter a NUMBER containing the amount you want to loan.")
            return;
        }
    }
}

function onClickWorkButton() {
    payBalance += 100
    if(payBalance >= outstandingLoan && outstandingLoan != 0) {
        workCardRepayLoanButton.disabled = false
    }
    workCardPayBalanceElement.innerText = payBalance + ' kr.'

}

function onClickBankButton() {
    let loanRepaymentAmount = 0
    let cleanIncome = 0

    if(loanTaken) {
        loanRepaymentAmount = payBalance * 0.1
    }

    if(loanRepaymentAmount > outstandingLoan) {
        cleanIncome = (loanRepaymentAmount - outstandingLoan) 
    }

    bankBalance += (payBalance - loanRepaymentAmount) + cleanIncome
    bankCardBalanceElement.innerText = bankBalance + ' kr.'
    
    if(bankBalance >= selectedLaptopPrice) {
        laptopCardBuyNowButton.disabled = false
    } else {
        laptopCardBuyNowButton.disabled = true
    }

    outstandingLoan = outstandingLoan - loanRepaymentAmount
    if(outstandingLoan <= 0) {
        loanTaken = false
        outstandingLoan = 0
        bankCardLoanRowElement.setAttribute('style', 'display:none')
        workCardRepayLoanRowElement.setAttribute('style', 'display:none')
    }
    bankCardOutstandingLoanElement.innerText = outstandingLoan + ' kr.'
    payBalance = 0
    workCardPayBalanceElement.innerText = payBalance + ' kr.'
}

function onClickRepayLoanButton() {
    let remainingCash = payBalance - outstandingLoan

    outstandingLoan = 0
    payBalance = 0
    bankBalance += remainingCash
    loanTaken = false
    bankCardBalanceElement.innerText = bankBalance + ' kr.'
    workCardPayBalanceElement.innerText = payBalance + ' kr.'
    bankCardOutstandingLoanElement.innerText = outstandingLoan + ' kr.'
    workCardRepayLoanButton.disabled = true
    workCardRepayLoanRowElement.setAttribute('style', 'display:none')
    bankCardLoanRowElement.setAttribute('style', 'display:none')
}

function onClickBuyNowButton() {

    if(bankBalance >= selectedLaptopPrice) {
        bankBalance = bankBalance - selectedLaptopPrice
        bankCardBalanceElement.innerText = bankBalance + ' kr.'
        for (var i = 0; i < laptopSelectElements.length; i++) {
            let option = laptopSelectElements.options[i]
            if(option.value == selectedLaptopId) {
                laptopSelectElements.removeChild(option)
            }
        }
        alert(`Congratulations! You are now the proud owner of the "${laptopTitleElement.innerText}"!`)
    } 
}

laptopSelectElements.addEventListener("change", onSelectChange)
bankCardLoanButton.addEventListener("click", onClickLoanButton)
workCardWorkButton.addEventListener("click", onClickWorkButton)
workCardBankButton.addEventListener("click", onClickBankButton)
workCardRepayLoanButton.addEventListener("click", onClickRepayLoanButton)
laptopCardBuyNowButton.addEventListener("click", onClickBuyNowButton)

